import Vue from 'vue';
import pdfjsLib from 'pdfjs-dist';

import PageComponent from '../../src/page/index.vue';
import workerSrc from '../../dist/pdf.worker';
import testPdf from '../../pdfs/test.pdf';

const Component = Vue.extend(PageComponent);

describe('Page component', () => {
  let vm;
  let testPage;
  pdfjsLib.PDFJS.workerSrc = workerSrc;

  const checkRendered = (done) => {
    if (vm.rendering) {
      setTimeout(() => {
        checkRendered(done);
      }, 100);
    } else {
      done();
    }
  };

  beforeEach((done) => {
    pdfjsLib.getDocument(testPdf)
      .then(pdf => pdf.getPage(1))
      .then((page) => {
        testPage = page;
        done();
      });
  });

  describe('render', () => {
    beforeEach((done) => {
      vm = new Component({
        propsData: {
          page: testPage,
          number: 1,
        },
      });

      vm.$mount();

      checkRendered(done);
    });

    it('renders first page', () => {
      expect(vm.$el.tagName).toBeDefined();
    });
  });
});
