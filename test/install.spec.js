import Vue from 'vue';
import PDFLab from '../src/index.vue';

Vue.use(PDFLab);

describe('Install function', () => {
  it('creates global component', () => {
    expect(
      Vue.options.components['pdf-lab'],
    ).not.toBeNull();
  });
});
