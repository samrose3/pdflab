const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: {
    pdflab: './index.js',
    'pdflab.min': './index.js',
    'pdf.worker': 'pdfjs-dist/build/pdf.worker.entry',
    'pdf.worker.min': 'pdfjs-dist/build/pdf.worker.entry',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].js',
    libraryTarget: 'umd',
    library: 'PDFLab',
    umdNamedDefine: true,
  },
  module: {
    loaders: [{
      test: /\.vue$/,
      use: ['vue-loader'],
      exclude: /node_modules/,
    }, {
      test: /\.js$/,
      use: ['babel-loader'],
      exclude: /node_modules/,
    }, {
      test: /\.gif$/,
      loader: 'url-loader',
      query: { mimetype: 'image/gif' },
    }, {
      test: /\.(worker.js|pdf)$/,
      loader: 'file-loader',
      exclude: /node_modules/,
    }],
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      include: /\.min\.js$/,
      minimize: true,
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
  ],
};
